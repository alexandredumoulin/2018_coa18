#!/usr/bin/env node

const spawn = require('child_process').spawn;
var command;
var cwd = process.env.NR_CWD || process.env.CWD;

command = spawn('node', [`${cwd}/node_modules/node-red/red.js`, '-v', '-u', `${cwd}`], {env: process.env, cwd: cwd});

command.stdout.on('data', (data) => {
  console.log(data.toString());
});

command.stderr.on('data', (data) => {
  console.log(data.toString());
});

command.on('close', (code) => {
  console.log(`child process exited with code ${code}`);
});
