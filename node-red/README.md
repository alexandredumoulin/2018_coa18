# Node-RED project template

## Installation

### Show Control Core

This is typically installed on the Synology. Inside `show_control_core` run `node download_showcontrol.js`.

### Show Control Agent

This is installed on each PC in an installation. Run `npm install` inside the show_control_agent folder.  You can choose which version of the show_control_agent you want to install in the `package.json` file.  If you want to update to a newer version, change the version in `package.json` and run `npm install` again.
