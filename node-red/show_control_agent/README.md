# Show Control Agent Deployment

## Background
The Show Control Agent is a Node-RED flow that needs to be deployed on every computer you wish to control and monitor.  This guide assumes you have copied the `project_template` repository and created a project repository from it.  For this guide let's assume your project is named `2017_bananaland`.

## Requirements
The computer should have the following tools installed before deploying the Show Control Agent:

* GIT: https://git-scm.com/downloads
* NodeJS: https://nodejs.org/en/download (Always choose the LTS version)
* Python 2.7.x: https://www.python.org/downloads/ (Make sure you add python.exe to path during install)

## SSH keys
Since most of the code is hosted in private GIT repositories, we need to install SSH deploy keys in order to pull the code. The following keys are necessary to install the project repository and the Show Control Agent, they can be found in 1Password:

* 2017_bananaland_rsa (This is your main project repository key, would be normally named according to your project)
* node-red-contrib-mf-display_rsa
* node-red-contrib-mf-hardware_monitor_rsa
* node-red-contrib-mf-lighting_rsa

The keys should be placed in your user's SSH config folder.  For example: `C:\Users\mofa\.ssh`.  Alongside the keys you also need a `config` file that should look like this:

```
Host 2017_bananaland
    Hostname github.com
    IdentityFile ~/.ssh/2017_bananaland_rsa

Host node-red-contrib-mf-hardware_monitor
    Hostname github.com
    IdentityFile ~/.ssh/node-red-contrib-mf-hardware_monitor_rsa

Host node-red-contrib-mf-lighting
    Hostname github.com
    IdentityFile ~/.ssh/node-red-contrib-mf-lighting_rsa

Host node-red-contrib-mf-display
    Hostname github.com
    IdentityFile ~/.ssh/node-red-contrib-mf-display_rsa

Host *
    StrictHostkeyChecking no
```

## Cloning the project

A folder named `show` should be created at the root of the `C` drive.  Using GIT Bash you can clone your project.  Make sure you `2017_bananaland` by the name of your project.

```bash
cd /c/show
git clone git@2017_bananaland:MomentFactory/2017_bananaland.git
```

## Installing the Show Control Agent

Once the project is cloned you can navigate to the Show Control Agent folder to install the package.  If you wish to install a specific version, you can change it in the `package.json` file.

```bash
cd C:\show\2017_bananaland\node-red\show_control_agent
npm install
```

## Automate startup

We will PM2 to automate the startup of the Show Control Agent. In the Windows command prompt type the following to install PM2:

```bash
npm install -g pm2
cd C:\show\2017_bananaland\pm2
```

In `pm2_win.json` change the settings for your project:

```json
{
    "apps": [{
        "name": "node-red",
        "script": "start_win.js",
        "env": {
            "PROJECT_TOPIC_NAME": "2017_bananaland",
            "IP_BROKER": "192.168.0.10",
            "IS_BACKUP": "false",
            "NR_CWD": "c:/show/2017_bananaland/node-red/show_control_agent"
        },
        "merge_logs": true
    }]
}
```

Once change you can start the file with PM2:

```bash
pm2 start pm2_win.json
pm2 save
```

Finally, invoke the run command window with Windows-R key (or by going to the start menu) and type `shell:startup`.  In the startup folder, create a file named `pm2.bat` with the following content:

```bash
pm2 resurrect
```

