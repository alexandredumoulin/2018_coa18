if EXIST generic_showcontrol_core_backend/nul (
   cd generic_showcontrol_core_backend
   git pull
   cd ..
) ELSE (
   git clone git@generic_repos_read:momentfactory/generic_showcontrol_core_backend.git
)

if EXIST generic_showcontrol_core_frontend/nul (
   cd generic_showcontrol_core_frontend
   git pull
   cd ..
) ELSE (
   git clone git@generic_repos_read:momentfactory/generic_showcontrol_core_frontend.git
)

if EXIST project-db-manager/nul (
   cd project-db-manager
   git pull
   cd ..
) ELSE (
   git clone git@generic_repos_read:MomentFactory/project-db-manager.git
)

cd generic_showcontrol_core_backend\nas-backend

::we rename flows.json so it doesn't affect the npm install inside the docker, but don't want it donwloaded
rename flows.json flows.json.final

cd ..\..\generic_showcontrol_core_frontend\theme-dashboard-default
call npm install

cd ..\..\project-db-manager\node-red
call npm install
