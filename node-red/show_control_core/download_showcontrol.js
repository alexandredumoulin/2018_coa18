#!/usr/bin/env node

'use strict';

const exec = require('child_process').execSync;
const fs = require('fs');
const os = require('os');

const keysToCheck = ['generic_repos_read_rsa',
    'node-red-contrib-mf-display_rsa',
    'node-red-contrib-mf-hardware_monitor_rsa',
    'node-red-contrib-mf-lighting_rsa',
    'node-red-contrib-mf-priority-queue_rsa'
];

let abort = false;
for (let key of keysToCheck) {
    if (!fs.existsSync(`${os.homedir()}/.ssh/${key}`)) {
        console.log(`SSH key ${key} missing from ${os.homedir()}/.ssh folder.`);
        abort = true;
    }
}
if (abort) {
    console.log('You have missing SSH keys, aborting install.');
    process.exit(1);
}

console.log('Attempting clone of generic_showcontrol_core_backend...');
exec('git clone git@generic_repos_read:MomentFactory/generic_showcontrol_core_backend.git');

console.log('Attempting clone of generic_showcontrol_core_frontend...');
exec('git clone git@generic_repos_read:MomentFactory/generic_showcontrol_core_frontend.git');

console.log('Building generic_showcontrol_core_backend...');
exec('cd generic_showcontrol_core_backend/nas-backend ; rm -rf .git ; npm install');

console.log('Building theme-dashboard-xa...');
exec('cd generic_showcontrol_core_frontend ; rm -rf .git');
exec('cd generic_showcontrol_core_frontend/theme-dashboard-xa ; npm install');

console.log('Building theme-dashboard-default...');
exec('cd generic_showcontrol_core_frontend/theme-dashboard-default ; rm -rf .git ; npm install');
