# Node-RED project template

## Installation

```
npm install
```
To run the project:
```
npm start
```
On an alternate port:
```
npm start -- -p 1882
```
A specific flow file:
```
npm start -- myFlow.json
```
To force an npm install:
```
npm start -- -f
```

