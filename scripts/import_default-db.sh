docker exec -it $1 mongoimport --db manager --collection categoryTypes --file /collections/categoryTypes.json
docker exec -it $1 mongoimport --db manager --collection genericEquipmentPresets --file /collections/genericEquipmentPresets.json
docker exec -it $1 mongoimport --db manager --collection projectEquipments --file /collections/projectEquipments.json
docker exec -it $1 mongoimport --db manager --collection projectSettings --file /collections/projectSettings.json
docker exec -it $1 mongoimport --db zones --collection generic_equipments_list --file /collections/zones.json