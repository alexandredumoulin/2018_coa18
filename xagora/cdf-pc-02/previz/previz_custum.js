var gLights = {
   "PointLight":{
   "type":"PointLight",
   "color":0xc9c9c9, 
   "visible":true,
   "src":{X:0, Y:50, Z:0},
   "intensity":0.5,
   "distance":0.0
   },
   "DirLight1":{
   "type":"DirectionalLight",
   "color":0xffffff,
   "visible":false,
   "src":{X:0, Y:100, Z:600},
   "intensity":1,
   "dest":{X:0, Y:10, Z:-100}
   },
   "SpotLight":
   {
      "type":"SpotLight",
      "color":0xc9c9c9, 
      "visible": true,
      "src":{X:-200, Y:150, Z:200}, 
      "intensity":1, 
      "dest":{X:50, Y:15, Z:0}, 
      "distance":500.0, 
      "angle":Math.PI/3,
      "exponent":10.0
   }
}