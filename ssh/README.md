# SSH configuration

This folder contains an example SSH config file.  The goal of the SSH config file is to allow pulling and pushing from different private Github repositories that use different SSH private keys.  Depending on your platform the ssh config file and keys can be located in different folders.  Typically:

Windows: `C:\Users\mofa\.ssh`  
Windows + Cygwin: `C:\cygwin64\home\mofa\.ssh`  
Mac: `/Users/mofa/.ssh`  
Synology: `/var/services/homes/mofa/.ssh`

When using Cygwin under windows it is recommended to have keys in both the Windows user's home and Cygwin user's home.  A junction can be created from the Cygwin `.ssh` folder to the Windows user's folder but not the other way around.

For each project or private repository you need to create a deploy key (replacing project_template with the name of your project, usually in the form of year_projectcode (2016_jfo)):

```
ssh-keygen -t rsa -b 4096 -C "project_template"
Enter a file in which to save the key (/Users/you/.ssh/id_rsa): project_template_rsa
Enter passphrase (empty for no passphrase): Leave empty

```

This generates a `project_template_rsa` private key and a `project_template_rsa.pub` public key.

On the repository web page in settings you then need to add the public key.  You will also need to store the public and private keys in 1Password, see your director for details if you do not have access.

## Example

Here is a real world example to make this more clear.  The [2015_chandler](https://github.com/MomentFactory/2015_chandler) project has 2 private node modules dependencies so we need those 2 keys (that can be found in 1Password) and the actual project key:

- 2015_chandler_rsa
- node-red-contrib-mf-lighting_rsa
- node-red-contrib-mf-calimotion_rsa

These privates keys need to be copied in your `~/.ssh` directory.  
To update the user rights on each of the files, use : 
```
rsa : chmod 600 filename
pub : chmod 644
config : chmod 600
.ssh : chmod 700
```
You would setup your `~/.ssh/config` file the following way:

```
Host 2015_chandler
	Hostname github.com
	IdentityFile ~/.ssh/2015_chandler_rsa

Host node-red-contrib-mf-lighting
	Hostname github.com
	IdentityFile ~/.ssh/node-red-contrib-mf-lighting_rsa

Host node-red-contrib-mf-calimotion
	Hostname github.com
	IdentityFile ~/.ssh/node-red-contrib-mf-calimotion_rsa
```

Once the private keys are copied, you can clone the repository:

```
cd ~/source
git clone git@2015_chandler:MomentFactory/2015_chandler.git
```

The dependencies section of you `package.json` file for your Node-RED project would look like this:

```
"dependencies": {
    "node-red": "0.13.4",
    "node-red-contrib-mf-lighting": "git+ssh://git@node-red-contrib-mf-lighting/MomentFactory/node-red-contrib-mf-lighting.git",
    "node-red-contrib-mf-calimotion": "git+ssh://git@node-red-contrib-mf-calimotion/MomentFactory/node-red-contrib-mf-calimotion.git",
    "node-red-node-physical-web": "0.0.12",
    "node-red-contrib-ui": "1.2.19"
}
```
