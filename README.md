# Moment Factory Project Template

A project template for Moment Factory projects

## Folder hierarchy

### max

Max/MSP files goes here.

### node-red

Node-RED related files go here. [Documentation.](node-red/README.md)

### pd

Pure Data files go here.

### pm2

pm2 configuration files goes here.

### python

python scripts go here.

### ssh

SSH config file goes here. [Documentation.](ssh/README.md) **NEVER COMMIT SSH KEYS TO A REPOSITORY**. 

## Git config
### Non-developer machines
if the repository was cloned on the project machine. set a global name and email.
>git config --global user.name [PROJECT_NAME/MACHINE]

>git config --global user.email service@momentfactory.com

You can create yourself a alias to quicky specify your name/email as the following.
>git config --local alias.commit-[your name] '-c "user.name=[your name]" -c "user.email=[your email] " commit'

Next time, instead of doing a 'git commit', use 'git commit-MyName;

